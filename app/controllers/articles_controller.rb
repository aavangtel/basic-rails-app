class ArticlesController < ApplicationController

	#http_basic_authenticate_with name: "alvaro", password: "test", except: [:index, :show]

	def index
		@articles = Article.all
	end

	def like
		@article = Article.find(params[:id])
		@article.likeinc
		redirect_to article_path(@article)
	end

	def edit
		@article = Article.find(params[:id])
	end

	def show
		@article = Article.find(params[:id])
	end

	def new
		@article = Article.new
	end

	def create

 		@article = Article.new(article_params)

 

 		if @article.save
			redirect_to action: 'show' , id: @article.id 
		else
			render 'new'
		end 
  		#redirect_to root_path
		#redirect_to @article
	end

 	def update 
		@article = Article.find(params[:id])
		if @article.update(article_params)
			redirect_to @article
		else
			render 'edit'
		end


	end

	def destroy
		@article = Article.find(params[:id])
		@article.destroy

		redirect_to articles_path
	end

	private

 	 def article_params

  	  	params.require(:article).permit(:title, :text)

 	 end


end
